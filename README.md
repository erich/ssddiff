SSDDiff — a diff for semistructured data
========================================

*Semistructured data* is a generic term for data that does have structure
information, while not being tabular or very tightly restricted. XML and HTML
data is the most prominent examples for this. You normally would not use this
term e.g. for database tables (which for example do not allow nesting of entries).

While this application currently only supports XML, the algorithms should be
able to process other semistructured data as well, for example JSON documents.

The approach used here is usually much slower than other well-known xmldiff
applications, however it produces better results in many "tricky" cases. You
say that other xmldiff applications try to do a syntactic diff, whereas xmldiff
tries to do a semantic diff.

In many use cases of XML, such as office documents, such tricky cases will
likely not occur at all, though. This method is mostly interesting when you
expect subtrees to be moved around, or the nesting to be changed.


News:
-----

**2018-03-27**: I migrated the project to git.

**2006-03-02**: Autotoolized sourcecode, should be even easier to compile now (read: `./configure && make`)

**2005-08-04**: Added a tarball for easier download (no subversion necessary) to the ssddiff alioth release area.
I also updated/continued the doxygen documentation.

**2005-08-04**: Just finished my talk at the conference. Slides are temporarily available here, and you can find the [article in the Proceedings.][extreme]

**2005-05-15**: There will be a talk about SSDDiff at [Extreme Markup Languages 2005](http://www.extrememarkup.org/extreme/) on Thursday August 4 2005.

**2005-05-10**: I started "doxyfying" the source code, so its easier to understand by others. Still lots of cleanup is needed... I hope I'll find time to do so.

**2005-02-10**: I flagged the current SVN version as 0.1. Consider this an alpha release of the application. I also clarified the licence by adding the GPLv2 as COPYING file and a corresponding line into each file.


State of the project
--------------------

Essentially, **the project is abandoned right now**, as I am working on quite different things these days.
However, the source code remains available, and you are welcome to investigate this further.

My project thesis and the [extreme markup languages publication][extreme] explains the reasoning, the benefits and the algorithm.

There was a presentation on Thursday August 4 2005 at Extreme Markup Languages 2005.


Contributing:
-------------

Since I no longer work on this myself, I really appreciate contributions,
especially if someone would be interested in bringing it to a beta release,
improving the fast mode, improving the output formats, adding a "patch" tool,
making it a library...

The only big demand I have for contributions: all contributed code must be
both GPL and LGPL licensed, so that I (or a future maintainer) can
change the licence to the LGPL in case this is considered desirable.
(As of now, Apache would also be an option, although I believe in copyleft.)

[extreme]: http://conferences.idealliance.org/extreme/html/2005/Schaffert01/EML2005Schaffert01.html
