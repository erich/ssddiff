/* ===========================================================================
 *        Filename:  util.h
 *     Description:  Header with utility classes and definitions
 * 
 *         Version:  $Rev$
 *         Changed:  $Date$
 *         Licence:  GPL (read COPYING file for details)
 * 
 *          Author:  Erich Schubert (eS), erich@debian.org
 *                   Institut für Informatik, LMU München
 * ======================================================================== */
#ifndef  UTIL_INC
#define  UTIL_INC

#include <string.h>
#include <unordered_map>
#include <string>

/** \brief Hashmap implementation to use */
#define hashmap std::unordered_map
/** \brief Hash implementation to use */
#define hashfun std::hash

/** \brief minimum macro */
#define MIN(a,b) (( (a<=b) ? a : b ))
/** \brief maximum macro */
#define MAX(a,b) (( (a>=b) ? a : b ))

/** \brief hash function for char* 'strings' */
struct hashstr {
	std::size_t operator()(const char* s) const {
		// Didn't work: return std::hash<std::string_view>()(std::string_view(s));
		return std::hash<std::string>()(std::string(s));
	}
};

/* vim:set noet sw=4 ts=8: */
#endif   /* ----- #ifndef UTIL_INC  ----- */
